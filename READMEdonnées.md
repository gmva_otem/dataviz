# Présentation du projet global

Le projet a pour objectif de suivre et de valoriser les données de
suivi des températures de l'eau de mer du Golfe du Morbihan. Ce suivi
pourra ainsi être intégré à l'Observatoire des effets du changement
climatique porté par Golfe du Morbihan Vannes Agglomération (GMVA).

Dans ce projet, il s'agit de développer une application Web
(responsive) pour visualiser les données collectées par les
capteurs de température. Les données devront être présentées de
manière simple pour que celles-ci puissent être facilement comprises
par les habitants de l'agglomération. Les données pourront être
présentées via des graphiques et des tableaux. Les dernières données
collectées et un historique des données devront être présentés. Les
données collectées seront stockées dans une base de données.

# Présentation du rôle de notre groupe : donnée

Au sein du projet OTEM, nous sommes dans la dernière ligne 
(la partie donnée et visualisation). Avant nous, les différents groupes
vont s'occuper de la mesure des données et de la transmission de ces données.
Notre rôle sera alors de récupérer les données des analyses et de les mettre en
corrélation avec des données supplémentaires (température de l'air et donnée de marée),
Ces données devront être en lien avec celles récupérées par les groupe de notre projet. 
C'est pourquoi nous devrons obtenir des données concernant les lieux de ces mesures
deux autres variables qui serviront alors à enrichir le travail du groupe
s'occupant de la visualisation qui devra alors mettre en avant les mesures effectuées.

# Nos outils groupe : donnée
Pour réaliser au mieux ce projet, nous allons utiliser des outils offrant des avantages
pour y parvenir, nous allons revenir sur ces différentes solutions utilisées.

Pour le code :
Nous utiliserons NodeJS/Javascript, cela va permettre de faciliter
l'utilisation des fichiers json récupérés avec les APIs et va aussi faciliter
pour les requêtes à ces APIs.

La base de données : 
Nous utiliserons influxDB, ce service offre la possibilité de lier
aux données une variable de temps très précise. 
Dans notre cas, cela sera un grand avantage pour apporter une précision
automatique aux données.

Nos APIs :

    température eau :
        L'API sera utilisée pour récupérer les mesures sera celle
        créée par un autre groupe OTEM.
    température air :
        Nous utiliserons openweatherMap qui permet un grand nombre d'appels
        et permet des requêtes simples et précises.
    marées :
        Pour ce choix, nous **utilisions** mareaAPI qui offre un service
        gratuit limité à 100 requêtes après la création d'un compte ce qui nous
        permettra de tester et d'avancer dans notre projet.

        Nous avons trouvé une solution plus durable:
            Nous scrappons les données de marée du site marée.info pour remplacer MareaAPI, ce qui nous permet de calculer les données à une heure précise.

Les différentes documentations pour le code et les outils utilisés se trouvent dans le dossier /Documentation
Les documents de gestion se trouvent dans le dossier /documents