var express = require('express');
const axios = require('axios');
require('dotenv').config();
var router = express.Router();
const grafanaUrl = 'http://host.docker.internal:3001';
const apiKey = process.env.API_URL;
const dashboardUid = 'OpJ9GXY4k';

/* GET home page. */
router.get('/', function(req, res, next) {

  const getDash = async (dashboardUID) => {
      let url = grafanaUrl + '/api/dashboards/uid/' + dashboardUID;
      let headers = {'Authorization':`Bearer ${apiKey}`};
      let r = await axios.get(url, headers, false);

      return r;
  }
  
  
  async function printDash() {
      try {
          const abcd = await getDash(dashboardUid);
          const panelIds = [];

          for (let i = 0; i < abcd.data.dashboard.panels.length; i++) {
            panelIds.push("http://localhost:3001/d-solo/"+dashboardUid+"/reportdash?orgId=1&from=1670843259000&to=1670846859000&panelId="+abcd.data.dashboard.panels[i].id);
          }

          console.log(panelIds);
          res.render('capt', { title: panelIds});
      } catch (error) {
          console.error(error);
      }
  }
  
  printDash();
  

});

module.exports = router;
