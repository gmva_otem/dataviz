var createError = require('http-errors');
require('dotenv').config();
var express = require('express');
const fs = require("fs");
const axios = require('axios');
var path = require('path');
var cookieParser = require('cookie-parser');
const os = require("os");
const session = require('express-session');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var capt1 = require('./routes/capt1');
var capt2 = require('./routes/capt2');
var capt3 = require('./routes/capt3');

var app = express();

app.use(session({
  
  // It holds the secret key for session
  secret: 'secret_key',

  // Forces the session to be saved
  // back to the session store
  resave: true,

  // Forces a session that is "uninitialized"
  // to be saved to the store
  saveUninitialized: true
}))

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/capt1', capt1);
app.use('/capt2', capt2);
app.use('/capt3', capt3);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



const API_KEY_NAME = 'api_key';
const GRAFANA_BASE_URL = 'http://admin:password@localhost:3000';

function setEnvValue(key, value) {

    // read file from hdd & split if from a linebreak to a array
    const ENV_VARS = fs.readFileSync("./.env", "utf8").split(os.EOL);

    // find the env we want based on the key
    const target = ENV_VARS.indexOf(ENV_VARS.find((line) => {
        return line.match(new RegExp(key));
    }));

    // replace the key/value with the new value
    ENV_VARS.splice(target, 1, `${key}=${value}`);

    // write everything back to the file system
    fs.writeFileSync("./.env", ENV_VARS.join(os.EOL));

}


async function createApiKey() {
  try {
    const response = await axios.post(
      `${GRAFANA_BASE_URL}/api/auth/keys`,
      {
        role: 'Admin',
        name: API_KEY_NAME,
      },
      {
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );
    
    var newApiUrl = response.data.key;
    console.log(`API key created: ${newApiUrl}`);

    setEnvValue("API_URL", newApiUrl);

  } catch (error) {
    console.error(`Failed to create API key: ${error.message}`);
  }
}

var x = process.env.API_URL;
if (x === "abcd"){
  createApiKey();
} 


module.exports = app;
