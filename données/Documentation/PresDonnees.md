# Projet OTEM
## Présentation des données utilisées

### tempEau : float
Cette donnée représente la température de l'eau récupérée par les capteurs des groupes précédents. Ils font donc remonter ces données puis ils les insèrent dans une API afin que l'on puisse la récupérer.
Nous allons la récupérer grâce à [cette API](https://gitlab.com/gmva_otem/network/-/tree/Zone_de_d%C3%A9p%C3%B4t-Partie_R%C3%A9seau_API/BDD).

### tempMeteo : float
Cette donnée représente la température extérieur au moment de la récupération de données des capteurs et à l'endroit correspondant à l'emplacement des capteurs.
Nous allons la récupérer grâce à [l'api OpenWeatherMap](https://openweathermap.org/guide).

### maree : float
Cette donnée représente la hauteur du niveau de l'eau au moment de la récupération de données des capteurs et à l'endroit correspondant à l'emplacement des capteurs.
Nous récupérions [l'api MareaAPI](https://api.marea.ooo/doc/v2#overview). Nous possèdons seulement 100 appels au total sur cette API.
Actuellement, nous scrappons les données de marée.info une fois par jour vers minuit, ce qui nous permet de recalculer les données de marée grâce à un calcul et obtenir une donnée qui s'approche de la vraie valeur.

D'autres données pourront être ajoutées dans le futur en fonction des besoins de visualisation des données.

## Exemple :
Voici un exemple avec des données aléatoires : (car toutes les APIs ne sont pas au point pour le moment)

![image](https://imgur.com/yFn7vbm.png)