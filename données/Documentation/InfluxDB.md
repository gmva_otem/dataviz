[![N|Solid](https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Influxdb_logo.svg/1200px-Influxdb_logo.svg.png)](https://nodesource.com/products/nsolid)


**InfluxDB** est un système de gestion de base de données orientée séries temporelles hautes performances, écrit avec le langage de programmation Go et distribué sous licence MIT.


### Pourquoi nous avons choisi InfluxDB

- NoSQL
- Insertion de temps automatique dans les données
- Une [documentation](https://docs.influxdata.com/influxdb/v2.6/) 

### Utilisation client graphique InfluxDB

##### Installer InfluxDB

```sh
wget -q https://repos.influxdata.com/influxdb.key
```

```sh
echo '23a1c8836f0afc5ed24e0486339d7cc8f6790b83886c4c96995b88a061c5bb5d influxdb.key' | sha256sum -c && cat influxdb.key | gpg --dearmor | sudo tee /etc/apt/trusted.gpg.d/influxdb.gpg > /dev/null
```

```sh
echo 'deb [signed-by=/etc/apt/trusted.gpg.d/influxdb.gpg] https://repos.influxdata.com/debian stable main' | sudo tee /etc/apt/sources.list.d/influxdata.list
```

```sh
sudo apt-get update && sudo apt-get install influxdb2
```



##### Lancer InfluxDB :


```sh
influxd
```

#### Se créer un compte utilisateur

![image](https://i.imgur.com/vNG895Y.png)

#### Installer les dépendances

```sh
npm install --save @influxdate/influxdb-client
```

#### Créer un fichier de configuration

Dans un fichier .env on renseigne les paramètres nécessaires au fonctionnement d'InfluxDB
```sh
//environnement de connexion InfluxDB
INFLUXDB_HOST=localhost:8086
INFLUXDB_TOKEN=12MDlSRqsdKv-GWcY3NucRIoRTZ1vVSSowcAzvHYd_jcsZjYLgrNNO5DjT6zeyMajw_MWlYJFZEaVJBivZJ0sg==
INFLUXDB_ORGANIZATION=Otem_données
INFLUXDB_BUCKET=otemTest
INFLUXDB_USERNAME=Enzo_LE-BODO
```

L'API token se crée dans l'interface InfluxDB:
![image](https://cdn.discordapp.com/attachments/895578510473711636/1064464499878416535/Capture_decran_du_2023-01-16_09-40-06.png)
![image](https://cdn.discordapp.com/attachments/895578510473711636/1064464719127261195/Capture_decran_du_2023-01-16_09-40-32.png)
![image](https://cdn.discordapp.com/attachments/895578510473711636/1064464541645283398/Capture_decran_du_2023-01-16_09-40-45.png)
![image](https://cdn.discordapp.com/attachments/895578510473711636/1064464789927108668/Capture_decran_du_2023-01-16_09-40-57.png)

#### Importer les modules nécessaires dans envoieDonnees.js

```sh
require('dotenv').config();
const { InfluxDB, Point } = require('@influxdata/influxdb-client');
const os = require('os');
```

#### Consulter les données
![image](https://cdn.discordapp.com/attachments/895578510473711636/1064466730480566302/Capture_decran_du_2023-01-16_09-50-14.png)

![image](https://cdn.discordapp.com/attachments/895578510473711636/1064466678840315945/Capture_decran_du_2023-01-16_09-49-50.png)

