# Compte rendu sur les optimisations réalisées et sur la sécurité

### Système pour que les clés d'API ne soient pas sur GitLab

On a mis en place un système permettant à l'utilisateur de renseigner ses clés d'APIs dans sa version du code et non depuis le dépôt GitLab.
En effet, l'utilisateur devra les renseigner dans un fichier .env dans /code comme cela:

```
KEY_OTEM = 11f398d8-4e90-4428-aff6-3e4ceb05cefb
KEY_METEO = votre-cléAPI-météo-ici
KEY_MAREE = votre-cléAPI-marée-ici
```

Le code de récupération des données des APIs va utiliser ces clés pour obtenir un accès aux APIs nécessaires au fonctionnement de l'application. Voici le paramètre permettant de récupérer les clés du fichier .env pour les utiliser dans une requête fetch d'une API:

```
fetch('http://api-container:3000/fetchLast?password=${process.env.KEY_OTEM}')
```
