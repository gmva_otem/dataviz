# Tutoriel pour lancer avec Docker

## Prérequis
- Avoir installé Docker ([voir notre documentation sur Docker](https://gitlab.com/gmva_otem/dataviz/-/blob/donn%C3%A9es/donn%C3%A9es/Documentation/Docker.md))

## Lancer l'application avec une base globale
```
sudo docker pull enzolb/otemdonnees:latest
sudo docker run appdonnees

```
Les données seront accessibles sur une base influxDB dont nous possedons les identifiants (n'hésitez pas à nous contacter)
Il faut aussi pour l'instant qu'une de nos machines tourne pour que cela fonctionne
Dans le futur, cette version aura une base hébergée sur un serveur.






## Lancer l'application avec votre base influxDB

## Cloner notre dépôt
1. Ouvrez votre terminal ou invite de commande.
2. Accédez à l'emplacement où vous souhaitez cloner le dépôt.
3. Exécutez la commande suivante pour cloner le dépôt :
```
git clone https://gitlab.com/gmva_otem/dataviz.git   
```
1. Accédez au répertoire cloné à l'aide de la commande :    
```
cd dataviz
```
2. Exécutez les commandes suivantes pour construire et lancer l'application :
```
sudo docker build && sudo docker up
```
Ne stoppez pas le processus pour le moment.

## Créer un compte et une clé d'API
1. Accédez à l'URL suivante dans votre navigateur : http://localhost:8088
2. Créez un compte et une clé d'API ([voir notre documentation pour plus de détails](https://gitlab.com/gmva_otem/dataviz/-/blob/donn%C3%A9es/donn%C3%A9es/Documentation/InfluxDB.md)).

## Modifier le fichier .env
1. Une fois le compte et la clé d'API créés, ouvrez le fichier .env avec un éditeur de texte.
2. Modifiez les champs suivants avec les informations correspondantes :
  - INFLUXDB_TOKEN avec votre clé d'API
  - INFLUXDB_ORGANIZATION avec le nom de votre organisation
  - INFLUXDB_BUCKET avec le nom du bucket où vous souhaitez stocker les données
  - INFLUXDB_USERNAME avec votre nom d'utilisateur

Exemple : 

![image](https://imgur.com/GLdKyCU.png)

3. Sauvegardez le fichier.

## Redémarrer l'application
1. Arrêtez le processus en utilisant `CTRL + C`.
2. Exécutez les commandes suivantes pour construire et lancer à nouveau l'application :
```
sudo docker build && sudo docker up
```
3. Si tout s'est bien passé, vous devriez obtenir un résultat similaire à celui de l'image ci-dessous.

![image](https://imgur.com/DjmqEOS.png)

Ce tutoriel devrait vous permettre de lancer l'application avec Docker en suivant les étapes décrites.
