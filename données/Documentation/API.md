# Récupérer l'API du groupe network en localhost

### Accès au script de récupération de l'API

Cloner le dépôt:
```
git clone https://gitlab.com/gmva_otem/network.git
```

 Ouvrir le dépôt dans visual et changer la branche. Il faut sélectionner Zone_de_dépôt-Partie_Réseau_API/BDD.

Lancer le client:
```
docker-compose build
docker-compose up
```
si une erreur indiquant que vous n'avez pas les droits essayez avec sudo

exemple :
```
sudo docker-compose build
sudo docker-compose up
```
si tout se passe bien vous devriez avoir ce message à la fin :
```
```
![image](https://media.discordapp.net/attachments/1030772564676575357/1065599715124715580/image.png)

En cas d'erreur de type :
```
```
![image](https://media.discordapp.net/attachments/1030772564676575357/1065597486590332938/image.png)

Il faudra alors modifier un peu de code ...
dans un premier temps dans src/api/app.js il faudra changer le port
exemple :
```
```
![image](https://media.discordapp.net/attachments/1065670297379950592/1065670409799868517/image.png)
```
```
et aussi dans src/docker-compose.yml
exemple :
```
```
![image](https://media.discordapp.net/attachments/1030772564676575357/1065598474306977842/image.png)

une fois réalisé relancer les commandes :

```
docker-compose build
docker-compose up
```

Dans un nouveau terminal Il faudra retrouver le fichier network
```
cd network/src
docker-compose ps
```


Réperer le port du container influxDB dans le résultat de la commande docker-compose ps:
```
```
![image](https://cdn.discordapp.com/attachments/972927193489498212/1065305336963141723/image.png)

On peut voir que ici le port est 32769. De ce fait, il faut lancer l'application en allant sur **http://localhost:port**
Il faut donc remplacer port par la valeur que vous obtenez

Ensuite il faut créer un compte InfluxDB sur l'application 
Il sera plus simple d'utiliser les informations suivantes pour diminuer les modifications par la suite :
```
```
![image](https://media.discordapp.net/attachments/1030772564676575357/1065600955904692324/image.png)

Une fois votre compte configuré il faudra vous créer une clé d'api
Si vous ne savez pas comment faire, c'est expliqué dans notre documentation influxDB

Il faut ensuite créer deux fichier config.js
un qui devra se trouver dans api/services/
exemple pour ce config.js
```
```
![image](https://media.discordapp.net/attachments/1030772564676575357/1065602206298013728/image.png)

Il faudra alors modifier les informations qui ne correspondent pas à votre environnement influxDB
notamment la clé d'API que vous venez de créer

Ensuite il faudra un deuxième fichier config.js
Il devra se trouver dans /scripts
exemple pour ce config.js
```
```
![image](https://media.discordapp.net/attachments/1030772564676575357/1065602716728037426/image.png)

Idem que pour le premier concernant les informations influxDB
- Remplacer "token" par le token de votre projet. Si vous faites partie du projet OTEM, contactez un membre du groupe.
```
```
![image](https://cdn.discordapp.com/attachments/972927193489498212/1065305893501153420/image.png)

### Accès à l'API

Une fois terminé 
```
docker-compose build
docker-compose up
```

Dans un navigateur accéder à:
```
localhost:3000/fetchAll
```

ou bien:
```
localhost:3000/fetchLast
```
Il faudra changer le port (3000) si vous avez changé