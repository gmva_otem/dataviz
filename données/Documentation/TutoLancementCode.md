# Projet OTEM
## Tutoriel : Lancement du code

Tout d'abord, notre code sert à récupérer les données de différentes APIs et à les croiser pour les insérer dans une base de données InfluxDB.

### Configuration avec InfluxDB

Pour ceci, il faudra déjà que vous ayez bien installé et configurer InfluxDB, nous avons aussi réaliser un [tutoriel pour l'installation d'InfluxDB](https://gitlab.com/gmva_otem/dataviz/-/blob/donn%C3%A9es/donn%C3%A9es/Documentation/InfluxDB.md).
Une fois que vous avez suivi ce tutoriel et bien installer InfluxDB. Vous pouvez télécharger notre code afin de l'éxécuter.(https://gitlab.com/gmva_otem/dataviz/-/tree/donn%C3%A9es/)

Pour le moment, il faudra un peu changer le code pour que cela fonctionne.
Entrez dans le fichier .env du fichier code et modifiez les valeurs suivantes pour que cela corresponde à votre configuration sur InfluxDB (expliqué dans le tutoriel plus haut).

Exemple : 
![image](https://imgur.com/H5ySrkS.png)

### Exécution du code

Une fois que vous avez configuré InfluxDB et le fichier .env. Vous pouvez tester si vous avez bien réaliser l'installation et la configuration avec la commande suivante :

```sh
npm start
```

(Cette commande sera utile pour tester avant de la lancer en continu grâce aux commandes présentes plus bas)

![image](https://imgur.com/GcLhr54.png)

Il faut ensuite que vous ouvrez un terminal et que vous vous rendiez dans la partie "/code".
Et écrivez la commande suivante sur ce terminal :
```sh
sudo npm install pm2 -g
```
Cette commande permet d'installer PM2. Cela vous permettra de faire tourner en continu même si vous fermez votre ordinateur ou quittez votre terminal.(https://pm2.keymetrics.io/)

Il faut ensuite lancer le code avec la commande :
```sh
pm2 start app.js
```

Et pour arrêter le fonctionnement du code :
```sh
pm2 stop app.js
```

![image](https://imgur.com/GQqcfwt.png)

Comme le montre l'image ci-dessus, on a lancé le code et donc les données vont être récupérées et ajoutées dans InfluxDB :

![image](https://imgur.com/igNspm1.png)
