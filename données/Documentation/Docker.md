![image](https://miro.medium.com/max/1400/1*OHTcNw50tddBUExCtpimGA.png)

Docker est un logiciel de virtualisation de conteneurs. Il permet de créer, déployer et exécuter des applications dans des conteneurs logiciels isolés. Les conteneurs sont des environnements logiciels légers qui peuvent être déplacés facilement d'un système à l'autre, tout en garantissant que l'application fonctionne de la même manière quelle que soit l'infrastructure utilisée. Les conteneurs Docker permettent aux développeurs de package leur application avec toutes les dépendances nécessaires, de sorte qu'elle puisse fonctionner sans problème sur n'importe quel système où Docker est installé.

#### Pourquoi utilisons-nous Docker?

Nous utilisons Docker afin de pouvoir récupérer l'API du groupe précédent (network) en local sur notre machine. De plus, Docker nous sert à fournir au groupe suivant (visualisation) notre base de données influxDB ainsi que le programme de récupération/croisement des données.

#### Installer Docker

Pour installer Docker Engine sur Ubuntu, vous pouvez suivre les étapes suivantes:

1. Mettre à jour les packages systèmes
```
sudo apt-get update
```
2. Installer les packages nécessaires pour l'utilisation d'HTTPS
```
sudo apt-get install apt-transport-https ca-certificates curl software-properties-common
```
3. Ajouter la clé GPG officielle de Docker
```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```
4. Ajouter le dépôt Docker à la liste des sources de paquetages
```
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
```
5. Mettre à jour la liste des paquetages disponibles
```
sudo apt-get update
```
6. Installer la dernière version stable de Docker Engine
```
sudo apt-get install docker-ce
```
7. Vérifier que Docker est en cours d'exécution
```
sudo systemctl status docker
```
8. Pour utiliser Docker sans avoir à utiliser sudo, ajoutez l'utilisateur courant au groupe Docker
```
sudo usermod -aG docker $USER
```
9. Redémarrer sa machine pour que la commande fasse effet
10. Vérifier la version de Docker installée
```
docker --version
```

Vous devriez maintenant être en mesure d'utiliser Docker sur votre système Ubuntu.
