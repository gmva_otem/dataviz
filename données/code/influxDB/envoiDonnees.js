require('dotenv').config();
const { InfluxDB, Point } = require('@influxdata/influxdb-client');
const os = require('os');
//réalisé à l'aide la documentation de INFLUXDB : https://docs.influxdata.com/influxdb/cloud/api-guide/client-libraries/nodejs/

/**
 * Classe permettant l'envoi et la récupération des données à la base de données influxDB
 */
class envoiDonnees{
  
    /**
     * Methode permettant l'envoi des données à la base de données
     */
    async envoi(){
    
      (async () => {
        if (!process.env.INFLUXDB_HOST) {
          console.error("ERROR : .env n'est pas créee ou correctement remplie"),
          process.exit(1)
        }


        // Initiate InfluxDB
        const influxdb = new InfluxDB({
          url: `http://${process.env.INFLUXDB_HOST}`,
          token: process.env.INFLUXDB_TOKEN
        })
    


        const writeApi = influxdb.getWriteApi(
          process.env.INFLUXDB_ORGANIZATION,
          process.env.INFLUXDB_BUCKET,
          'ns'
        )
        

        var donnee=require('../modele/Donnees')
        var idCapteur=donnee.donneeIdCapteur
        var tempMeteo=donnee.donneeMeteo
        var tempEau=donnee.donneeOtem
        var mareeS=donnee.donneeMareeScrap

        const mesure = new Point('mesure2')
        //il faudra mettre l'heure de la mesure (celle de l'api Otem)
          .measurement(`${idCapteur}`)
          .floatField('tempMeteo', tempMeteo)
          .floatField('tempEau', tempEau)
          .floatField('maree',mareeS )
          
        writeApi.writePoint(mesure)
        writeApi.flush()

        await writeApi.close()
      })().catch(error => {
        console.error('')
        console.error('ERROR:')
        console.error(error)
        process.exit(1)
      });
    }
    /**
     * Methode permettant la récupération des données de la base de données (ici pour vérifier que les données sont bien envoyées)
     */
    async recupere() {
      console.log('Nouvelle mesure')
      const influxdb = new InfluxDB({
        url: `http://${process.env.INFLUXDB_HOST}`,
        token: process.env.INFLUXDB_TOKEN
      })
  
      const queryApi = influxdb.getQueryApi(
        process.env.INFLUXDB_ORGANIZATION,
      );

      var donnee=require('../modele/Donnees')
      var idCapteur=donnee.donneeIdCapteur
      // va renvoyer les dernières valeurs que l'on vient d'ajouter dans la base de données avec envoi()
      //permet de vérifier que les données sont bien envoyées
      // la structure peut être gardée pour récupér les données dans un format différent il faudra alors modifier la requête "fluxQuery"
      const fluxQuery = `from(bucket: "${process.env.INFLUXDB_BUCKET}") |> range(start: -1h) |> filter(fn: (r) => r._measurement == "mesure2") |> filter(fn: (r) => r._field == "tempMeteo" or r._field == "tempEau" or r._field == "maree" or r._field == "id_Capteur") |> last()`
      
      const fluxObserver = {
        next(row, tableMeta) {
          const o = tableMeta.toObject(row)
          console.log(
            `time=${o._time}; ${o._field}=${o._value}`
          )
        },
        error(error) {
          console.error(error)
          console.log('\nFinished ERROR')
        },
        complete() {
          
        }
      }
      queryApi.queryRows(fluxQuery, fluxObserver)

    }

      
}

module.exports = new envoiDonnees()
