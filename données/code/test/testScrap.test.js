const assert = require('assert');
const Scrap = require('../modele/Scrap.js');
const Donnees = require('../modele/Donnees.js');

const idPort = 107;

describe('Tests pour le scraping des données de marée 12 avril 2023 (les valeurs change chaque jour)', function() {
  it('Test de la fonction getTideLevel avec une heure de marée de 12:10', async function() {
    const idPort = 107;
    const mapMaree = await new Scrap().scrapData(idPort);
    const time = '12:10';
    Donnees.keepDataMap(mapMaree, idPort);
    const result = new Scrap().getTideLevel(mapMaree, time, idPort);
    assert.strictEqual(result, 2.54);
  });

  it('Test de la fonction getTideLevel avec une heure de marée de 23:59', async function() {
    const idPort = 107;
    const mapMaree = await new Scrap().scrapData(idPort);
    const time = '23:59';
    Donnees.keepDataMap(mapMaree, idPort);
    const result = new Scrap().getTideLevel(mapMaree, time, idPort);
    assert.strictEqual(result, 2.58);
  });

  it('Test de la fonction getTideLevel avec une map personnalisée de données de marée et une heure de marée de 19:00', function() {
    const idPort = 107;
    const map = new Map();
    map.set('16:53', '4');
    map.set('22:05','1.1');
    const time = '19:00';
    const result = new Scrap().getTideLevel(map, time, idPort);
    assert.strictEqual(result, 2.95);
  });

  it('Test de la fonction getTideLevel avec une map vide', function() {
    const idPort = 107;
    const emptyMap = new Map();
    const availableTime = '12:00';
    assert.throws(() => {
      new Scrap().getTideLevel(emptyMap, availableTime, idPort);
    }, Error);
  });

  // Test de la récupération des données de marée pour le port avec l'ID 107
  describe('Test de la récupération des données de marée', function () {
    it('Vérifie que les données de marée sont bien récupérées pour le port avec l\'ID 107', function () {
      return new Scrap().scrapData(idPort)
        .then((mapMaree) => {
          console.log(mapMaree);
          assert.ok(mapMaree.size > 0);
        });
    });
  });

  // Tests de la fonction getTideLevel
  describe('Test de la fonction getTideLevel', function () {
    // Test de la fonction getTideLevel avec une heure de marée de 12:10
    it('Vérifie que la fonction getTideLevel retourne bien un niveau de marée pour une heure de marée de 12:10', function () {
      const time = '12:10';
      return new Scrap().scrapData(idPort)
        .then((mapMaree) => {
          Donnees.keepDataMap(mapMaree, idPort);
          const level = new Scrap().getTideLevel(mapMaree, time, idPort);
          console.log(level);
          assert.strictEqual(typeof level, 'number');
        });
    });

    // Test de la fonction getTideLevel avec une heure de marée de 23:59
    it('Vérifie que la fonction getTideLevel retourne bien un niveau de marée pour une heure de marée de 23:59', function () {
      const time = '23:59';
      return new Scrap().scrapData(idPort)
        .then((mapMaree) => {
          Donnees.keepDataMap(mapMaree, idPort);
          const level = new Scrap().getTideLevel(mapMaree, time, idPort);
          console.log(level);
          assert.strictEqual(typeof level, 'number');
        });
    });
  });
});