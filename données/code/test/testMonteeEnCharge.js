require('dotenv').config();
const { InfluxDB, Point } = require('@influxdata/influxdb-client');
const os = require('os');

// fonction pour envoyer les données à InfluxDB
async function envoiDonnees(idCapteur, tempMeteo, tempEau, mareeS) {
  if (!process.env.INFLUXDB_HOST) {
    console.error('ERROR: .env n\'est pas créé ou correctement rempli');
    process.exit(1);
  }

  // initialisation du client InfluxDB
  const influxdb = new InfluxDB({
    url: `http://${process.env.INFLUXDB_HOST}`,
    token: process.env.INFLUXDB_TOKEN
  });

  const writeApi = influxdb.getWriteApi(
    process.env.INFLUXDB_ORGANIZATION,
    process.env.INFLUXDB_BUCKET,
    'ns'
  );

  // ajout des tags par défaut pour le hostname
  writeApi.useDefaultTags({
    location: os.hostname()
  });

  // création d'un point pour la nouvelle mesure
  const mesure = new Point('mesure2')
    .measurement('mesure2')
    .floatField('id_Capteur', idCapteur)
    .floatField('tempMeteo', tempMeteo)
    .floatField('tempEau', tempEau)
    .floatField('maree', mareeS);

  // envoi du point à InfluxDB
  writeApi.writePoint(mesure);
  writeApi.flush();

  // fermeture du client InfluxDB
  await writeApi.close();
}


// fonction pour générer un nombre aléatoire entre min et max
function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}

describe('envoiDonnees', () => {
  const nbRequests = 10; // nombre de requêtes à envoyer
  const interval = 0; // intervalle entre chaque envoi de requête (en ms)
  const startTime = Date.now(); // temps de début d'exécution
  let count = 0; // compteur de requêtes envoyées

  // boucle pour envoyer les requêtes
  for (let i = 0; i < nbRequests; i++) {
    it(`envoie la requête ${i + 1}`, async () => {
      const idCapteur = getRandomNumber(1, 10);
      const tempMeteo = getRandomNumber(-10, 40);
      const tempEau = getRandomNumber(-5, 25);
      const mareeS = getRandomNumber(0, 5);

      await envoiDonnees(idCapteur, tempMeteo, tempEau, mareeS);
      count++;

      console.log(`Requête ${count} envoyée.`);

      // si toutes les requêtes ont été envoyées, on affiche le temps d'exécution
      if (count === nbRequests) {
        const endTime = Date.now(); // temps de fin d'exécution
        const executionTime = (endTime - startTime) / 1000; // temps d'exécution en secondes
        console.log(`Toutes les requêtes ont été envoyées en ${executionTime} secondes.`);
      }
    });
  }
});
