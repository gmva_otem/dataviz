const assert = require('assert');
const appelAPI = require('../modele/AppelAPI');

describe('Test de la boucle d\'appels aux API', function () {
  it('La boucle d\'appels aux API démarre et s\'arrête sans erreur', function (done) {
    var appel = new appelAPI();
    console.log("Démarrage de la boucle d'appels aux API...");
    appel.demarrer();

    // Arrêt de la boucle après une durée de 30 secondes
    setTimeout(function() {
      appel.arreter();
      console.log("Boucle d'appels aux API terminée avec succès !");//il faut appeler avec un intervalle de moins 1000ms dans AppelAPI pour que le test soit validé car mocha va jusqu'à 2000ms
      done();
    }, 1000);
  });
});