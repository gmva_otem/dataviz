const fetch = require('node-fetch');
const assert = require('assert');

// Test de l'API openweathermap
fetch('https://api.openweathermap.org/data/2.5/weather?q=Vannes,fr&appid=c21a75b667d6f7abb81f118dcf8d4611&units=metric')
  .then(function(res) {
    if (res.ok) {
      return res.json();
    }
  })
  .then(function(value) {
    

    // Ajout des tests
    describe('Test de l\'API openweathermap', function () {
      it('Vérifie que la température renvoyée par l\'API est bien un nombre', function () {
        assert.strictEqual(typeof value.main.temp, 'number');
      });
      it('Vérifie que le code de réponse de l\'API est bien 200', function () {
        assert.strictEqual(value.cod, 200);
      });
    });

  })
  .catch(function(err) {
    console.error("Erreur lors de l'appel à l'API :", err);
  }); 