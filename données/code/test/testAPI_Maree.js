/**
 * Code obsolète qui était utile lorsqu'on utilisait marea.api mais nous avons trouver une autre solution plus efficace (scraping)
 * Dans ce fichier on va tester l'API qui va permettre de récupérer les données des marées
 * Ce test est simplement un appel pour vérifier que l'API fonctionne
 

 const fetch = require('node-fetch')
 var server = 'https://api.marea.ooo/v2/tides?duration=1440&interval=60&latitude=44.414&longitude=-2.097&model=FES2014&datum=MSL'

var headers = {}
headers = {
  
  "x-marea-api-token": "5d62a958-f806-42be-8c7d-ded40313b150",
  "Accept": "application/json"
}


fetch(server, { method: 'GET', headers: headers})
.then((res) => {
    console.log(res)
    return res.json()
})
.then((json) => {
    console.log(json)
})
*/