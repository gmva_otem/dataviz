const getDonnees = require('./getDonnees')
const envoiDonnees = require('../influxDB/envoiDonnees')

/**
 * Classe permettant de faire des appels à l'api toutes les x temps 
 */
class AppelAPI {

  /**
   * Constructeur de la classe AppelAPI
   * @param intervalId - l'id de l'intervalle avant l'arret de la boucle d'appel (peu ne pas être renseigné pour une boucle infinie)
   * C'est d'ailleurs le cas pour notre projet mais cet intervalle est utile pour nos tests
   */
  constructor() {
    this.intervalId = null
  }

  /**
   * methode permettant de lancer les appels pour la récupération des données(est appelée si la méthode synchronisation renvoi true)
    */
  demarrer() {
    this.intervalId = setInterval(async () => {
      if(await getDonnees.synchronisation()) {
        await getDonnees.traitement()
        await envoiDonnees.envoi()
        await envoiDonnees.recupere()
      }
  
      
    }, 3000) //10000 = 10s
  }

  

  /**
   * methode permettant d'arreter la boucle d'appel
   * @param intervalId - l'id de l'intervalle avant l'arret de la boucle d'appel
   */

  arreter() {
    clearInterval(this.intervalId)
  }
}

module.exports = AppelAPI
