/**
 * Classe Donnees permettant de stocker les données des api
 * Elle permet aussi de stocker des données temporairement et de permettre le lien entre les différentes classes
 * Elle est une classe singleton
 */
class Donnees {
    /**
     * Constructeur de la classe Donnees
     * @param donneeMeteo - la température de la mer
     * @param donneeMaree - la hauteur de la marée
     * @param donneeOtem - la température de l'eau
     * @param donneeHeure - l'heure de la mesure
     * @param donneeDate - la date de la mesure
     */
    constructor() {
        this.donneeMeteo=undefined
        this.donneeMaree=undefined//on garde cette valeur pour la version avec mareaAPI
        this.donneeOtem=undefined
        this.donneeHeure=undefined
        this.donneeDate=undefined
        this.donneeIdCapteur=undefined
        this.donneeMareeScrap=undefined
        this.mapMaree=undefined
        this.mapMaree2=undefined
        this.mapMaree3=undefined
        this.compt=0
        //on pourra changer les noms des ports on aura peut-être que celui de vannes
        //liste des ports disponibles proches de vannes
        /**
         * vannes
         * arradon
         * saint armel
         * saint goustan
         * 
         * Ces donnees representent les dernieres hautes/basses marées donnees de la veille
         * elles serviront à calculer pour le jour actuel les mesure en debut de journee
         * (pour avoir un interval comme on ne récupère que les données d'une journée)
         */

        this.dataPorts={
            "port1":{
                dataTMP:{
                    lastH:undefined,
                    lastM:undefined
                },
                lastH:undefined,
                lastM:undefined,
                startH:undefined,
                startM:undefined,
            },
            "port2":{
                dataTMP:{
                    lastH:undefined,
                    lastM:undefined
                },
                lastH:undefined,
                lastM:undefined,
                startH:undefined,
                startM:undefined,
            }
        }

    }
    /**
     * methode permettant de stocker les données de l'api météo en fonction de l'api appelée
     * @param value - les données de l'api (json)
     * @param api - l'api appelée
     * 1 pour l'api maree
     * 2 pour l'api meteo
     * 3 pour l'api otem
     * 4 pour pour garder la dernière haute ou basse marée du jour précédent
     * 5 pour garder la donnée de la marée scrapée
     */
    convertTimestampToHHMM(timestamp) {
        const dateObj = new Date(timestamp);
        const hours = dateObj.getUTCHours();
        const minutes = dateObj.getUTCMinutes();
        const formattedHours = hours < 10 ? `0${hours}` : hours;
        const formattedMinutes = minutes < 10 ? `0${minutes}` : minutes;
        return `${formattedHours}:${formattedMinutes}`;
      }
      
    keepData(value,api) {
        if(api==2) {
            this.donneeMeteo=value.main.temp
        }else if(api==1) {
            this.donneeMaree=value.heights[0].height
        }else if(api==3) {
            if(typeof(value.temp)=="string"){
                value.temp=parseFloat(value.temp)
            }
            this.donneeOtem=value.temp
            const dateObj = new Date(value.time);
            const hours = dateObj.getUTCHours();
            const minutes = dateObj.getUTCMinutes();
            const formattedHours = hours < 10 ? `0${hours}` : hours;
            const formattedMinutes = minutes < 10 ? `0${minutes}` : minutes;
            this.donneeHeure=`${formattedHours}:${formattedMinutes}`
            this.donneeDate=value.date
            this.donneeIdCapteur=value.idSensor
        }else if(api==4){
            if(value.idPort==107){//vannes
                //va permettre de garder la dernière haute ou basse marée du jour précédent (et son horaire)
                if(this.dataPorts.port1.dataTMP.lastH!=undefined){
                    this.dataPorts.port1.lastH=this.dataPorts.port1.dataTMP.lastH
                    this.dataPorts.port1.lastM=this.dataPorts.port1.dataTMP.lastM
                    this.dataPorts.port1.dataTMP.lastH=value.lastH
                    this.dataPorts.port1.dataTMP.lastM=value.lastM
                }else{
                    this.dataPorts.port1.dataTMP.lastH=value.lastH
                    this.dataPorts.port1.dataTMP.lastM=value.lastM
                    this.dataPorts.port1.lastH=value.lastH
                    this.dataPorts.port1.lastM=value.lastM
                    this.dataPorts.port1.startH=value.HStart
                    this.dataPorts.port1.startM=value.MStart
                }
            }else{
                //va permettre de garder la dernière haute ou basse marée du jour précédent (et son horaire)
                if(this.dataPorts.port2.dataTMP.lastH!=undefined){
                    this.dataPorts.port2.lastH=this.dataPorts.port2.dataTMP.lastH
                    this.dataPorts.port2.lastM=this.dataPorts.port2.dataTMP.lastM
                    this.dataPorts.port2.dataTMP.lastH=value.lastH
                    this.dataPorts.port2.dataTMP.lastM=value.lastM
                }else{
                    this.dataPorts.port2.dataTMP.lastH=value.lastH
                    this.dataPorts.port2.dataTMP.lastM=value.lastM
                    this.dataPorts.port2.lastH=value.lastH
                    this.dataPorts.port2.lastM=value.lastM
                    this.dataPorts.port2.startH=value.HStart
                    this.dataPorts.port2.startM=value.MStart
                }
            }

        }
        else if(api==5){

            this.donneeMareeScrap=value
        }   
        
        
    }
    keepDataMap(value,port){
        if(port==107){//vannes
            this.mapMaree=value
        }else if(port==108){
            this.mapMaree2=value
        }
    }
    
}
module.exports=new Donnees()