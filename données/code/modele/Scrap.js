//modules nécessaires pour le scraping
const cheerio = require("cheerio");

const axios = require("axios");
/**
 * Classe permettant de récupérer les données de marées
 * via le site maree.info, on le requête avec l'id du port
 * Une seul fois par jour et on recalcule le niveau de la marée à une heure donnée
 * Le code de la classe reste peu lisibles car il s'agit d'un code de scraping
 * il y a donc beaucoup de manipulations de données de changement de format
 * C'est un point que nous allons améliorer dans notre code final
 */
class Scrap{
    constructor(){
        this.url = "http://maree.info/";
    }

    
    /**
     * Permet d'obtenir des données de marées qui serviront 
     * ensuite à un calcul plus précis
     * @param {*} idPort id du port (voir marée.info ex 107 pour vannes)
     * @returns le map() des hautes et basses marées d'une journée
     */
    scrapData= async function(idPort) {
        try {
          const response = await axios.get(this.url+idPort);
          const $ = cheerio.load(response.data);
            const table = $("table");
            const tableRows = table.find("tr");
            const tableData = tableRows.find("td");
            const tableDataArray = tableData.toArray();
            const tableDataArrayText = tableDataArray.map((element) => {
                return $(element).text();
                }
                
            );
            var tide = [];
            for (let i = 0; i < 11; i++) {
                if (tableDataArrayText[i].includes("m")) {
                    tide.push(tableDataArrayText[i]);
                }
            }
            var tmpTide=tide;
            var getTide= tide[tide.length-2].split("m");
            getTide.push(tmpTide[tmpTide.length-1].substr(0,6).split("m")[0]);
            getTide= getTide.filter((element) => element!=="");
            
            

            var heures = [];
            for (let i = 0; i < 10; i++) {
                if (tableDataArrayText[i].includes("h")) {
                    heures.push(tableDataArrayText[i]);
                }

            }  
            var heureTmp=heures;
            heures=heures[heures.length-2];//on garde que l'avant derniere (correspond à aujourd'hui)
            heures+=[heureTmp[heureTmp.length-1].substr(0,5)];//on ajoute la première valeure du lendemain
            
            const matches = heures.match(/\d{2}/g);
            const formatted = matches.map((num, i) => {
            return i % 2 === 0 ? `${num}:` : `${num},`;
            }).join('');
            var getHeures = formatted.split(",");
            getHeures= getHeures.filter((element) => element!=="");
            

            var mapHeureTide = new Map();
            for (let i = 0; i < getHeures.length; i++) {
                mapHeureTide.set(getHeures[i],getTide[i].replace(",","."));
            }
            var donnees=require("./Donnees.js");
            var dureeMaree=this.subtractTime(getHeures[1].replace(":","."),getHeures[0].replace(":","."))
            var hStart=(this.subtractTime(getHeures[0].replace(":","."),dureeMaree))
            hStart=this.addTime(hStart,'00:02')
            /*
            console.log(dureeMaree,'dureeMaree',idPort)
            console.log(getHeures,'getHeures',idPort)
            console.log(mapHeureTide,'mapHeureTide',idPort)
            console.log('hStart',hStart,idPort)
            console.log(idPort)
            */
            var data={
                "idPort":idPort,
                "lastH":getHeures[getHeures.length-1],
                "lastM":getTide[getTide.length-1],
                "HStart":hStart.replace(".",":"),
                "MStart":this.getTideLevel(mapHeureTide,hStart,idPort),
            }
            donnees.keepData(data,4);
            
            return mapHeureTide;

        } catch (error) {
          console.error(error);

        }
    }
    /**
     * Permet de soustraire deux float en gardant le format hh.mm
     * avec hh<=24 et mm<=60
     * @param {*} time le temps de départ
     * @param {*} subtract le temps à soustraire
     * @returns 
     */
    subtractTime=function(time, subtract) {
    
        const timeDate = new Date();
        const [hours, minutes] = time.split('.').map(Number);
        const [subtractHours, subtractMinutes] = subtract.split('.').map(Number);
      
        timeDate.setHours(hours);
        timeDate.setMinutes(minutes);
        timeDate.setHours(timeDate.getHours() - subtractHours);
        timeDate.setMinutes(timeDate.getMinutes() - subtractMinutes);
      
        let resultHours = timeDate.getHours();
        let resultMinutes = timeDate.getMinutes();
      
        if (resultMinutes < 0) {
          resultMinutes = 60 + resultMinutes;
          resultHours--;
        }
        if (resultHours < 0) {
          resultHours = 24 + resultHours;
        }
      
        return `${resultHours}.${resultMinutes.toString().padStart(2, '0')}`;
      }
      /**
       * permet d'ajouter deux float en gardant le format hh.mm 
       * avec hh<=24 et mm<=60
       * @param {*} time le temps de départ
       * @param {*} add le temps à ajouter
       * @returns le temps total
       */
      addTime=function(time, add) {
        const timeDate = new Date();
        const [hours, minutes] = time.split('.').map(Number);
        const [addHours, addMinutes] = add.split('.').map(Number);
      
        timeDate.setHours(hours);
        timeDate.setMinutes(minutes);
        timeDate.setHours(timeDate.getHours() + addHours);
        timeDate.setMinutes(timeDate.getMinutes() + addMinutes);
      
        let resultHours = timeDate.getHours();
        let resultMinutes = timeDate.getMinutes();
      
        if (resultMinutes > 59) {
          resultMinutes = resultMinutes - 60;
          resultHours++;
        }
        if (resultHours > 23) {
          resultHours = resultHours - 24;
        }
      
        return `${resultHours}.${resultMinutes.toString().padStart(2, '0')}`;
    }
    /**
     * Permet de convertir un float(minutes même >60) en hh.mm
     * @param {*} minutes le nombre de minutes à convertir
     * @returns le temps en hh.mm
     */
    minutesToHoursAndMinutes=function (minutes) {
        let hours = Math.floor(minutes / 60);
        let remainingMinutes = minutes % 60;
        remainingMinutes=Number(remainingMinutes).toFixed(0);
        remainingMinutes=remainingMinutes.toString().replace(".","");
        return `${hours.toString().padStart(2, '0')}.${remainingMinutes.toString().padStart(2, '0')}`;
      }

    //time doit toujours être sous la forme hh:mm et pas h:mm
    /**
     * Les données seront approximatives( on va essayer de trouver une méthode plus précise)
     * @param {*} tides le map() des hautes et basses marées
     * @param {*} time le temps où l'on veut une valeur de la marée
     * @returns le niveau de la marée à un temps donnée (ce niveau reste approximatif + ou - 0.1 près)
     */
    getTideLevel = function(tides, time, idPort){
        let hInf=null;
        let hSup=null;
        for (const [t, l] of tides) {
            var tmp=t.replace(":",".");
            if(tmp<time){
                if(hInf === null || tmp>hInf){
                    hInf=tmp;
                }
            }
            if(tmp>time){
                if(hSup === null || tmp<hSup){
                    hSup=tmp;
                }
            }
            if(hInf !== null && hSup !== null){
                break;
            }
        }
        if(hSup===null){
            for(let key of tides.keys()){
                hSup=key;
            }   
        }
        if(hInf===null){
            var donnees=require("./Donnees.js");
            if(donnees.compt===0){
                if(idPort==107){
                    hInf=donnees.dataPorts.port1.startH.replace(":",".");
                    var niveauHInf=donnees.dataPorts.port1.startM;
                }else{
                    hInf=donnees.dataPorts.port2.startH.replace(":",".");
                    var niveauHInf=donnees.dataPorts.port2.startM;
                }
            }else {
                if(idPort==107){
                    hInf=donnees.dataPorts.port1.lastH.replace(":",".");
                    var niveauHInf=donnees.dataPorts.port1.lastM;
                }else{
                    hInf=donnees.dataPorts.port2.lastH.replace(":",".");
                    var niveauHInf=donnees.dataPorts.port2.lastM;
                }
                
            }
            hSup=hSup.replace(".",":");
            hInf=hInf.replace(".",":");
            if(hSup !== null && hInf !== null){
                if(tides.get(hSup)>niveauHInf){
                    niveauSup=tides.get(hSup);
                    niveauInf=niveauHInf;
                }else{
                    niveauSup=niveauHInf;
                    niveauInf=tides.get(hSup);
                }
            }
        }else{
            hSup=hSup.replace(".",":");
            hInf=hInf.replace(".",":");
            
            
            var niveauSup=100;
            var niveauInf=-100;
            if(hSup !== null && hInf !== null){
                if(tides.get(hSup)>tides.get(hInf)){
                    niveauSup=tides.get(hSup);
                    niveauInf=tides.get(hInf);
                }else{
                    niveauSup=tides.get(hInf);
                    niveauInf=tides.get(hSup);
                }
            }
        }

            
        

        var evolution= niveauSup-niveauInf;
        var hMaree=this.subtractTime(hSup.replace(":","."),hInf.replace(":","."));
        if(hMaree.length===5){
            hMaree=hMaree.substr(0,2)*60+Number(hMaree.substr(3,2));
        }else{
            hMaree=hMaree.substr(0,1)*60+Number(hMaree.substr(2,2));
        }
        var hMaree=hMaree/6

        hMaree=this.minutesToHoursAndMinutes(hMaree);


        //console.log("Niveau bas",niveauInf)
        //console.log("Niveau haut",niveauSup)
        //console.log("Evolution",evolution)
        var douzieme=evolution/12;
        /**
         * https://www.lavoilepourlesnuls.com/calcul-de-marees-simple/
         * On sait que les marées durent environ 6h
         * Lors de la 1ère heure marée l’eau monte (ou descends) de 1 douzième.
            Lors de la 2ème heure marée l’eau monte (ou descends) de 2 douzièmes.
            Lors de la 3ème heure marée l’eau monte (ou descends) de 3 douzièmes.
            Lors de la 4ème heure marée l’eau monte (ou descends) de 3 douzièmes.
            Lors de la 5ème heure marée l’eau monte (ou descends) de 2 douzièmes.
            Lors de la 6ème heure marée l’eau monte (ou descends) de 1 douzième.
         */
        var tab=[1,2,3,3,2,1];
        var tide=niveauInf;
        if(tides.get(hSup)==niveauInf){
            douzieme=-douzieme;
            tide=niveauSup;
        }
        var min=this.subtractTime(time.replace(':','.'), hMaree);
        var compt=0;
        tide=parseFloat(tide)
        hInf=hInf.replace(':','.')

        for (let i = 0; parseFloat(hInf)<parseFloat(min); i++) {

            tide=tide+((douzieme)*tab[i]);
            hInf=this.addTime(hInf,hMaree);
            compt=i;
            
        }
        
        time=time.replace(':','.')
        var reste=this.subtractTime(time,hInf);

        if(parseFloat(hInf)<parseFloat(time)){
            tide+=(tab[compt+1]*douzieme*parseFloat(reste))/parseFloat(hMaree);
        }
        return Number((tide).toFixed(2));//pour arrondir à deux chiffres après la virgule

    }
    

    
}



module.exports = Scrap;