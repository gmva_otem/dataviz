const fetch = require('node-fetch')
require('dotenv').config();


/**
 * classe permettant de récupérer les données et les ajouter à une instance unique de la classe Donnees (singleton)
 */

class getDonnees {
    /**
     * methode permettant l'appel de l'api maree, elle va ajouter les données à une instance unique de la classe Donnees
     * Cette solution n'est plus utilisée on scrap maintenant les données
     */

    apiMaree = async function() {
        
        var server = 'https://api.marea.ooo/v2/tides?duration=1440&interval=60&latitude=47.833314&longitude=-2.5&model=FES2014&datum=MSL'
        var headers = {}
        headers = {
        
        "x-marea-api-token": process.env.KEY_MAREE,
        "Accept": "application/json"
        }
        await fetch(server, { method: 'GET', headers: headers})
        .then(function(res) {
            if (res.ok) {
                return res.json()
            }
        })
        .then(function(value) {
            var donnees=require('./Donnees.js')
            
            donnees.keepData(value,1)
            return value
        })
    
        .catch(function(err) {
            console.error(err)
            
        });
        
    }
    /**
     * methode permettant l'appel de l'api meteo, elle va ajouter les données à une instance unique de la classe Donnees
     */
    apiMeteo = async function() {  
        await fetch(`https://api.openweathermap.org/data/2.5/weather?q=Vannes,fr&appid=${process.env.KEY_METEO}&units=metric`)
        .then(function(res) {
            if (res.ok) {
                return res.json()
            }
        })
        .then( function(value) {
            var donnees=require('./Donnees.js')
            donnees.keepData(value,2)

            return value
            
        })
        .catch(function(err) {
            console.error(err)
            
        });
        
        
    }
    /**
     * permet d'avoir des heures aléatoires
     * @returns une heure aléatoire entre >=10h
     */
    generateRandomTime=function() {
        const hour = Math.floor(Math.random() * 14 + 10).toString().padStart(2, '0');
        const minute = Math.floor(Math.random() * 60).toString().padStart(2, '0');
        return `${hour}:${minute}`;
    }
    /**
     * methode permettant l'appel de l'api température de l'eau (otem), elle va ajouter les données à une instance unique de la classe Donnees
     */
    
    apiOtem = async function() {
        
        
        //code permettant de récupérer les données de l'api otem (en commentaire car l'api n'est pas toujours disponible)
        await(fetch(`http://api-container:3000/fetchLast?password=${process.env.KEY_OTEM}`)
        .then(function(res) {
            if (res.ok) {
                return res.json()
            }
        })
        .then(function(value) {
            var donnees=require('./Donnees.js')
            donnees.keepData(value.data[0],3)
            return value
        })
        .catch(function(err) {
            console.error(err)

        }));
        /*
        
         //code permettant de simuler les données de l'api otem (en commentaire car l'api est maintenant disponible)
        
        
            var donnees=require('./Donnees.js')
            //json qui permet de simuler les données de l'api otem
            var value={
                "idSensor" : Math.round(Math.random()*1),
                "idFloater" : Math.round(Math.random()*1),
                "date" : Date.now()+7200000,
                "time" : Date.now()+7200000,
                "temp" : Math.round(Math.random()*20)
            }
            donnees.keepData(value,3)
        */
    
        
        
    }

    
    /**
    méthode permettant d'appeler la méthode Scrape et de calculer le niveau de la marée en fonction de l'heure de l'appel
    */

    scraping = async function (idPort) {
        const Scrap = require('./Scrap.js');
        const Donnees = require('./Donnees.js');
        const mapMaree = await new Scrap().scrapData(idPort);
        Donnees.keepDataMap(mapMaree, idPort);
    }
    
    /**
    méthode permettant de calculer le niveau de la marée en fonction de l'heure actuelle
    */
    
    getTideLevel = async function(idPort) {
        try{
            var donnees=require('./Donnees.js')
            if(idPort==107) {//vannes
                var mapMaree=donnees.mapMaree
            }else if(idPort==108) {//pour l'instant nous ne savons pas quels ports seront analysés
                var mapMaree=donnees.mapMaree2
            }

            var time=donnees.donneeHeure
            var Scrap=require('./Scrap.js')
            donnees.keepData(new Scrap().getTideLevel(mapMaree,time,idPort),5)
        }catch(err) {
            console.log(err)
        }

    }

        
    /**
     * Va vérifier si une nouvelle mesure a été transmise par l'api otem
     * @returns true si les données ont changé, false sinon
     */
    synchronisation = async function() {
        var donnees=require('./Donnees.js')
        var date=donnees.donneeDate
        var heure=donnees.donneeHeure
        var idCapteur=donnees.donneeIdCapteur
        await this.apiOtem()
        if((date!=donnees.donneeDate && heure!=donnees.donneeHeure)||(idCapteur!=donnees.donneeIdCapteur)) {
            return true
        }else {
            return false
        }
    }
    
    

    /**
     * methode permettant de lancer les appels des 3 apis ( est appelée si la méthode synchronisation renvoi true)
     */
    traitement= async function() {
        await this.apiMeteo()
        var donnees=require('./Donnees.js')
        if(donnees.donneeIdCapteur=='01'){
            var idPort=107
        }else if(donnees.donneeIdCapteur=='02'){
            var idPort=108
        }
        //il faudra faire en fonction du capteur récupéré avec l'api Otem
        const now = new Date();
        var compt=donnees.compt
        if(compt==0) {
            var test=true
            await this.scraping(107)
            await this.scraping(108)
            compt++
            
            donnees.compt=compt
        }else if(compt>=1) {
            if((now.getHours()=='00' && (now.getMinutes()=='01'||now.getMinutes()=='02'))&& test==true) {
                await this.scraping(107)
                await this.scraping(108)
                test=false
            }
        }
        if(test==false && (now.getHours()=='02')){
            test=true
        }
        if(compt>1000){
            compt=1
            donnees.compt=compt
        }
        await this.getTideLevel(idPort)
    }

}
module.exports=new getDonnees()
